package Chapter11;

public class TestInheritance {
    public static void main(String[] args) {
        Accounts[] accounts=new Accounts[3];
        accounts[0]=new SavingAccount("abc",2);
        accounts[1]=new SavingAccount("xyz",4);
        accounts[2]=new CurrentAccount("pqr", 6);
 
        for(int i=0;i<accounts.length;i++)
        {
            accounts[i].addInterest();
            System.out.println(accounts[i].getName()+" has "+ accounts[i].getBalance()+" balance");
        }
 
    }
    
}