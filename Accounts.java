package Chapter11;

public abstract class Accounts {
    private String name;
    private double balance;

    public Accounts(String name, double balance)
    {
        this.name=name;
        this.balance=balance;
    }

    public abstract void addInterest();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
        
    }
    
}